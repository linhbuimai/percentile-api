import json
from os import listdir

import requests
from numpy import percentile as pct

from src.responseMessages import *

def post_req (doc_link):
    url = "http://127.0.0.1:5000/v1/percentile"
    file = {'file': (doc_link, open(doc_link, 'rb'), 'text/csv')}
    return requests.post (url=url, files=file).text

if __name__ == "__main__":
    path = "./tests/test_data/"
    ls_file = sorted (listdir(path))
    for file in ls_file[0:4]:
        assert (post_req (f"{path}{file}") == INVALID_PCT)
        pass
    for file in ls_file[4:10]:
        assert (post_req (f"{path}{file}") == INVALID_POOL)
        pass
    for file in ls_file[10:12]:
        assert (post_req (f"{path}{file}") == INVALID_DOC)
        pass
    for file in ls_file[12:]:
        r = json.loads (post_req (f"{path}{file}"))
        r = r.get ("value")
        f = open (f"{path}{file}")
        js = json.load(f)
        f.close ()
        assert (r == pct (js.get("pool"), js.get("percentile")))
        pass