import json
import math

from flask import Flask, request

from src.responseMessages import *

app = Flask (__name__)

@app.route ("/v1/percentile", methods=["POST"])
def main ():
    """Receive a document, parse it then return percentile of a list from that document
    """
    try:
        file = request.files.get("file")
        js_input = json.loads (str(file.read (), 'utf-8'))
    except json.decoder.JSONDecodeError:
        return INVALID_DOC
    else:
        ls_num = js_input.get("pool")
        pct = js_input.get("percentile")
        return calc_percentile (ls_num, pct)

def calc_percentile (lsNum, pct):
    """Calculate percentile of a list of values
    
    Params:
    
        lsNum: list of numeric values with no None value
        pct: pecentile number, range from 0 to 100
    
    Return: Percentile of this list
    """
    # Check if percentile number is valid
    try:
        assert (0 <= pct <= 100)
    except (TypeError, AssertionError):
        return INVALID_PCT
    # Check if list is valid 
    try:
        assert (lsNum != None)
        assert (len(lsNum) > 1)
        assert (all(isinstance(item, (int, float)) for item in lsNum) == True)
    except AssertionError:
        return INVALID_POOL
    else:
        lsNum = sorted (lsNum)
        num = (len(lsNum) - 1) * pct/100
        floor = int(math.floor (num))
        ceil = int(math.ceil (num))
        return {'value': lsNum[floor] + (lsNum[ceil] - lsNum[floor])*(num-floor)}

if __name__ == "__main__":
    app.run(debug=True)