## Percentile-API
This is a simple REST API with a POST endpoint which receives a JSON in the form of a document with two fields: a list and a quantile (in percentile form).
Example: 
```
{
    "pool": [1,3, 5, 3, 2, 6, 32, 52],
    "percentile": 99.5
}
```

## Installation
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install dependencies of this project.

```
pip install -r requirement.txt
```
## Usage

Navigate to project folder and run `python main.py`

**Sample request:**

```
curl -F 'file=@./tests/test_data/case1_1.json' http://127.0.0.1:5000/v1/percentile
curl -F 'file=@./tests/test_data/case1_2.csv' http://127.0.0.1:5000/v1/percentile
```
**Sample response:**

```
{
  "value": 51.3
}
```

## Note

- This percentile calculation follows formula in this site: [CalculatorSoup](https://www.calculatorsoup.com/calculators/statistics/percentile-calculator.php)
- Document should contain valid json format, with two required arguments:
    + `pool`: List of numeric values with more than 1 element
    + `percentile`: A number, from 0 to 100